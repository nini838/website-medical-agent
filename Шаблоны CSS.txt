height:  // Высота
width:   // Ширина

left: 	 // Лево
right:   // Право
top:	   // Верх
bottom:  // Низ

max-				// Ограничение
	margin:   // Внешний отступ
	padding:  // Внутрений отступ

z-index:   	// Уровень
position:  	// Позиция
	relative 	// Свободен для изменений
	absolute 	// Фиксация по сайту (без прокрутки)
	fixed 	 	// Фиксация по экрану (с прокруткой)
	static 	 	// По умолчанию
	sticky   	// Фиксация по родителю

display:		 					// Флекс объявление
	flex  							// По горизонту
	block 							// Блокировать
flex-direction: 			// Ось
	column							// Сменить главную ось (Вертикаль)
	row									// Сменить вторичную ось (Горизонт)
	row-reverse					// Сменить вторичную ось
	column-reverse			// Сменить вторичную ось с главной
justify-content: 		  // Выравнивание по главной оси
align-items: 				  // Выравнивание по перпендикулярной главной оси
	flex-start					// Все слева
	flex-end						// Все справа
	center							// Все по центру
	space-between				// По всему обьекту
	space-around				// По всему обьекту с отступом
	stretch   					// На весь обьем ячейки
	baseline						// Стандартная позиция
	align-self 					// По отдельности
flex-basis: 					// Отвечает за общий размер
flex-grow: (0-1)			// Относительное значение по отношению к другим
flex-shrink: (0-1)		// Насколько можно уменьшиться в размере
flex-wrap:
flex-flow:

order: 0,1,2,3,4      // Порядок по которому идут блоки
align-self 					// По отдельности

background-color:				 		// Цвет
opacity: (0-1)							// Прозрачность
box-shadow: 0 0 10px #000 	// Тень (ширина, высота, толщина, цвет)
border-top: 5px solid red 	// Граница (ширина, тип, цвет)
border-radius: 15px;        // Скруглить границу (ширина)
outline											// Внутренняя граница

transition:									// Анимация плавная и в течение некоторого времени
transition-property: 				// Указывает список свойств
transition-duration:				// Назначение продолжительности
transition-timing-function: // Точки ускорения и замедления
transition-delay: 					// Задержка времени



	margin: opx auto; // Внешний отступ. ставит по центру
	text-alight: center // поставит текст по центру
	font-size: 18px // обьем шрифта
	font-weight: 700 // Жирность шрифта
	line-height:
	before // Псевдо-элемент


	background: url('../') no-repeat
		no-repeat
		repeat x
		repeat y
		space
		round
